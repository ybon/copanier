Salut salut,

Voici le résumé de votre commande «{{ delivery.producer }}»

Produit | Prix unitaire | Quantité

{% for product in delivery.products %}
{% if order.get_quantity(product) %}
{{ product.name }} | {{ product.price }} € | {{ order.get_quantity(product) }}
{% endif %}
{% endfor %}

Total: {{ order.total(delivery.products) if order else 0 }} €

Livraison: {{ delivery.where }}, le {{ delivery.from_date|date }} de {{ delivery.from_date|time }} à {{ delivery.to_date|time }}

{% if delivery.is_open %}
Tu peux la modifier (jusqu'au {{ delivery.order_before|date }}) en cliquant ici:

https://{{ request.host }}/livraison/{{ delivery.id }}/commander

{% endif %}

Bonne journée!
