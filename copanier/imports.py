import csv
from zipfile import BadZipFile

from openpyxl import load_workbook, Workbook

from .models import Product


PRODUCT_FIELDS = {"ref", "name", "price"}


def products(delivery, data):
    if isinstance(data, str):
        reader = products_from_csv(delivery, data)
    elif hasattr(data, "filename") and data.filename.endswith(".csv"):
        reader = products_from_csv(delivery, data.read().decode())
    else:
        reader = products_from_xlsx(delivery, data)
    delivery.products = []
    for row in reader:
        delivery.products.append(Product(**row))
    delivery.persist()


def products_from_xlsx(delivery, data):
    if not isinstance(data, Workbook):
        try:
            data = load_workbook(data)
        except BadZipFile:
            raise ValueError("Impossible de lire le fichier")
    rows = list(data.active.values)
    if not rows:
        raise ValueError
    headers = rows[0]
    headers = [h.strip() if h else "" for h in headers]
    if not set(headers) >= PRODUCT_FIELDS:
        raise ValueError("Colonnes obligatoires: name, ref, price")
    for row in rows[1:]:
        raw = {k: v for k, v in dict(zip(headers, row)).items() if v}
        if not raw:
            continue
        yield raw


def products_from_csv(delivery, data):
    reader = csv.DictReader(data.splitlines(), delimiter=";")
    if not set(reader.fieldnames) >= PRODUCT_FIELDS:
        raise ValueError("Colonnes obligatoires: name, ref, price. "
                         "Assurez-vous que le délimiteur soit bien «;»")
    yield from reader
